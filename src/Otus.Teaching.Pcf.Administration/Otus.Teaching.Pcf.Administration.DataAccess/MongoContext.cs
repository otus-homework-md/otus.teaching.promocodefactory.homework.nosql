﻿using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess
{
    

    public class MongoContext
    {
        //private MongoUrl mongoUrl;
        //private MongoClient mongoClient;
        public IMongoDatabase Database { get; private set; }

        public MongoContext(string url, string db) 
        {
            //var mongoUrl= new MongoUrl(url);
            var mongoClient= new MongoClient(url);
            Database = mongoClient.GetDatabase(db);
        }
    }
}
